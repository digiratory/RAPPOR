If you use this repository in your scientific research, please cite our paper:

D. I. Kaplun, D. V. Gnezdilov, G. A. Efimenko, A. M. Sinitca and V. V. Gulvanskiy, "Research and implementation of the algorithm for data de-identification for Internet of Things," 2017 IEEE II International Conference on Control in Technical Systems (CTS), 2017, pp. 363-366, doi: 10.1109/CTSYS.2017.8109568.

```bibtex
    @INPROCEEDINGS{KaplunRAPPOR,
        author={Kaplun, Dmitriy I. and Gnezdilov, Denis V. and Efimenko, George A. and Sinitca, Aleksandr M. and Gulvanskiy, Vyacheslav V.},
        booktitle={2017 IEEE II International Conference on Control in Technical Systems (CTS)}, 
        title={Research and implementation of the algorithm for data de-identification for Internet of Things}, 
        year={2017},
        volume={},
        number={},
        pages={363-366},
        doi={10.1109/CTSYS.2017.8109568}
    }
```
