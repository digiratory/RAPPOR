@echo off
:: !!Do not change mode value!!!
set mode=1
set file_name=..\sourceData\domain_name.csv
set numBits=16
set probabilityF=0.5
set probabilityP=0.4
set probabilityQ=0.6
set numCohorts=32
set numBloomHashes=8
:: If the number of devices is too small, execution errors may occur
set numDevices=500
set numReportPerDevice=1000
@echo on
java -jar RapporTest.jar -m %mode% -f %file_name% -B %numBits% -F %probabilityF% -P %probabilityP% -Q %probabilityQ% -C %numCohorts% -H %numBloomHashes% -D %numDevices% -R %numReportPerDevice%

python preprocessing.py
pause