# -*- coding: utf-8 -*-
"""
Created on Mon Sep  4 21:15:57 2017

@author: Александр
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def bin_string_to_bin_array(string):
    arr = np.fromstring(string, dtype=np.uint8)-48
    return np.asarray(arr, dtype=np.int)


numBits = 8
numCohorts = 4
file = '../genData/data.csv'
print("Data file: " + file)
data = pd.read_csv(file,sep=';', encoding='utf-8', dtype={'Report':str})
data_np = data.as_matrix()
#data_np = data_np [0:1000,:]
hist = np.histogram(data_np[:,0], bins=numCohorts)
hist = hist[0]
'''
plt.hist(data_np[:,0], bins=32)  # arguments are passed to np.histogram
plt.title("Histogram RAPPOR reports")
plt.show()
'''

countWrite = np.zeros((numCohorts,numBits), dtype=np.int)
for i in range(data_np.shape[0]):
    d = bin_string_to_bin_array(data_np[i,1])  
    if d.size == numBits:
        data_np[i,1] = d
        for j in range(numBits):
            countWrite[data_np[i,0],j] += d[j]

dataToSave = ""
for i in range(numBits):
    dataToSave += "\"V"+str(i+1)+"\" "
dataToSave += "\n"
for i in range(numCohorts):
    dataToSave  += "\""+str(i+1)+"\" "
    dataToSave  += str(hist[i]) + " "
    for j in range(numBits):
        dataToSave  += str(countWrite[i,j]) + " "
    dataToSave  += "\n"    
    
file = open('../genData/countsWrite.txt', 'w')
file.write(dataToSave)
file.close()
print("Counts write into" + '../genData/countsWrite.txt')
