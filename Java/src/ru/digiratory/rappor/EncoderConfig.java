/**
 * 
 */
package ru.digiratory.rappor;

/**
 * @author Aleksandr Sinitca
 *
 */
public class EncoderConfig {
	String userSecret;
	String encoderId;
	int numBits;
	double probabilityF;
	double probabilityP;
	double probabilityQ;
	int numCohorts;
	int numBloomHashes;

	/**
	 * 
	 * @param userSecret
	 * @param encoderId
	 * @param numBits
	 * @param probabilityF
	 * @param probabilityP
	 * @param probabilityQ
	 * @param numCohorts
	 * @param numBloomHashes
	 */
	public EncoderConfig(String userSecret, String encoderId, int numBits, double probabilityF, double probabilityP,
			double probabilityQ, int numCohorts, int numBloomHashes) {
		super();
		this.userSecret = userSecret;
		this.encoderId = encoderId;
		this.numBits = numBits;
		this.probabilityF = probabilityF;
		this.probabilityP = probabilityP;
		this.probabilityQ = probabilityQ;
		this.numCohorts = numCohorts;
		this.numBloomHashes = numBloomHashes;
	}

	/**
	 * @return the userSecret
	 */
	public String getUserSecret() {
		return userSecret;
	}

	/**
	 * @return the encoderId
	 */
	public String getEncoderId() {
		return encoderId;
	}

	/**
	 * @return the numBits
	 */
	public int getNumBits() {
		return numBits;
	}

	/**
	 * @return the probabilityF
	 */
	public double getProbabilityF() {
		return probabilityF;
	}

	/**
	 * @return the probabilityP
	 */
	public double getProbabilityP() {
		return probabilityP;
	}

	/**
	 * @return the probabilityQ
	 */
	public double getProbabilityQ() {
		return probabilityQ;
	}

	/**
	 * @return the numCohorts
	 */
	public int getNumCohorts() {
		return numCohorts;
	}

	/**
	 * @return the numBloomHashes
	 */
	public int getNumBloomHashes() {
		return numBloomHashes;
	}

	/**
	 * @param userSecret
	 *            the userSecret to set
	 */
	public void setUserSecret(String userSecret) {
		this.userSecret = userSecret;
	}

	/**
	 * @param encoderId
	 *            the encoderId to set
	 */
	public void setEncoderId(String encoderId) {
		this.encoderId = encoderId;
	}

	/**
	 * @param numBits
	 *            the numBits to set
	 */
	public void setNumBits(int numBits) {
		this.numBits = numBits;
	}

	/**
	 * @param probabilityF
	 *            the probabilityF to set
	 */
	public void setProbabilityF(double probabilityF) {
		this.probabilityF = probabilityF;
	}

	/**
	 * @param probabilityP
	 *            the probabilityP to set
	 */
	public void setProbabilityP(double probabilityP) {
		this.probabilityP = probabilityP;
	}

	/**
	 * @param probabilityQ
	 *            the probabilityQ to set
	 */
	public void setProbabilityQ(double probabilityQ) {
		this.probabilityQ = probabilityQ;
	}

	/**
	 * @param numCohorts
	 *            the numCohorts to set
	 */
	public void setNumCohorts(int numCohorts) {
		this.numCohorts = numCohorts;
	}

	/**
	 * @param numBloomHashes
	 *            the numBloomHashes to set
	 */
	public void setNumBloomHashes(int numBloomHashes) {
		this.numBloomHashes = numBloomHashes;
	}
}
