package ru.digiratory.rappor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import javax.swing.SpinnerDateModel;

import ru.digiratory.datasource.CSVDataSource;
import ru.digiratory.datasource.DataSource;
import ru.digiratory.rappor.device.DeviceBuilder;
import ru.digiratory.rappor.device.manager.DeviceManager;
import ru.digiratory.rappor.device.manager.SpeedTestDeviceManager;
import ru.digiratory.rappor.device.manager.TextFileDeviceManager;
/**
 * RAPPOR is a novel privacy technology that allows inferring statistics about populations while preserving the privacy of individual users.
 * 
 * Programm for generation test reports and map file for RAPPOR
 * 
 * References:
 * 
 * <a href="http://dx.doi.org/10.21609/jiki.v10i1.440">De-Identification Technique for IoT Wireless Sensor Network Privacy Protection</a>
 * 
 * <a href="http://arxiv.org/abs/1407.6981">RAPPOR: Randomized Aggregatable Privacy-Preserving Ordinal Response</a>
 * 
 * <a href="http://arxiv.org/abs/1503.01214">Building a RAPPOR with the Unknown: Privacy-Preserving Learning of Associations and Data Dictionaries</a>
 * 
 * Used open source code:
 * 
 * <a href="https://github.com/google/rappor">Google RAPPOR</a>
 * 
 * 
 * @author amsinitca@etu.ru Sinitca Aleksandr
 *
 */
public class RapporTest {
	
	/**
	 * A non-decreasing version number.
	 *
	 * <p>
	 * The version number should increase any time the Encoder has a user-visible
	 * functional change to any of encoding algorithms or the interpretation of the
	 * input parameters.
	 */
	public static final long VERSION = 1;
	
	/**
	 * Command line key for choose work mode
	 */
	static final String CL_ARG_MODE = "-m";
	
	/**
	 * Command line key for choose data source file or URI
	 */
	static final String CL_ARG_FILE = "-f";
	
	/**
	 * Command line key for choose number of bits in the RAPPOR-encoded report.
	 */
	static final String CL_ARG_NUM_BITS = "-B";
	
	/**
	 * Command line key for choose RAPPOR "f" probability.
	 */
	static final String CL_ARG_PROBABILITY_F = "-F";
	
	/**
	 * Command line key for choose RAPPOR "p" probability.
	 */
	static final String CL_ARG_PROBABILITY_P = "-P";
	
	/**
	 * Command line key for choose RAPPOR "q" probability.
	 */
	static final String CL_ARG_PROBABILITY_Q = "-Q";
	
	/**
	 * Command line key for choose number of cohorts used for cohort assignment
	 */
	static final String CL_ARG_NUM_COHORT = "-C";
	
	/**
	 * Command line key for choose number of hash functions used forming the Bloom filter encoding of a
	 * string.
	 */	
	static final String CL_ARG_NUM_BLOOM_HASHES = "-H";
	
	/**
	 * Command line key for choose number of virtual device for generating RAPPOR-report
	 * 
	 * WARNING! If you choose too little value, program can work unstable
	 */
	static final String CL_ARG_NUM_DEVICES = "-D";
	
	/**
	 * Command line key for choose number of RAPPOR-report per virtual device
	 */
	static final String CL_ARG_NUM_REPORT_PER_DEVICE = "-R";

	
	
	static int mode = 1;
	static String file_name = "sourse.csv";
	static int numBits = 8;
	static double probabilityF = 0.5;
	static double probabilityP = 0.4;
	static double probabilityQ = 0.6;
	static int numCohorts = 4;
	static int numBloomHashes = 3;
	static int numDevices = 200;
	static int numReportPerDevice = 2000;
	
	static String map_file_name = "../genData/mapWrite.txt";
	/**
	 * Command line arguments:
	 * 
	 * -m work mode(Default: 1) 1 saving data to csv
	 * 
	 * -f data source file or URI (Default: domain_name.csv)
	 * 
	 * -B numBits (Default: 16)
	 * 
	 * -F probabilityF (Default: 0.5)
	 * 
	 * -P probabilityP (Default: 0.4)
	 * 
	 * -Q probabilityQ (Default: 0.4)
	 * 
	 * -C numCohorts (Default: 32)
	 * 
	 * -H numBloomHashes (Default: 8)
	 * 
	 * -D numDevices (Default: 1000)
	 * 
	 * -R numReportPerDevice (Default: 1000)
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		HashMap<String, String> argMap = new HashMap<>();
		if (args.length % 2 == 0) {
			for (int i = 0; i < args.length; i += 2) {
				argMap.put(args[i], args[i + 1]);
			}
			analiseCommandLine(argMap);
		} else {
			System.out.println("Args error: use all default");
		}

		reportParams();

		EncoderConfig cnfg = new EncoderConfig("Blotto", "Foo", numBits, probabilityF, probabilityP, probabilityQ,
				numCohorts, numBloomHashes);

		DeviceManager manager;
		CSVDataSource csvds = new CSVDataSource(file_name, 0);
		switch (mode) {
		case 0:
			manager = new TextFileDeviceManager(numBits, "../genData/data.csv");
			manager.addDevice(DeviceBuilder.getDevices(cnfg, numDevices, "Dev", new DataSource(), 0, numReportPerDevice,
					manager));
			manager.startDevices();
			break;
		case 1:
			manager = new TextFileDeviceManager(numBits, "../genData/data.csv");
			manager.addDevice(DeviceBuilder.getDevices(cnfg, numDevices, "Dev", csvds, 0, numReportPerDevice, manager));
			manager.startDevices();
			while (manager.getCountRunningDevices() > 0) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			String mapWrite = manager.getDictionaryTable();
			FileWriter fos;
			File outputFile = new File(map_file_name);
			try {
				fos = new FileWriter(outputFile, false);
				fos.write(mapWrite);
				fos.flush();
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Raw report writed into: " + "../genData/data.csv");
			System.out.println("Map writed into: " + map_file_name);
			break;
		case 2:
			manager = new SpeedTestDeviceManager(numBits, "../genData/data.csv");
			
			manager.addDevice(DeviceBuilder.getDevices(cnfg, numDevices, "Dev", csvds, 0, numReportPerDevice, manager));
			manager.startDevices();
			while (manager.getCountRunningDevices() > 0) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			((SpeedTestDeviceManager) manager).closeFile();
			System.out.println("Raw report writed into: " + "../genData/data.csv");
			break;
		default:
			break;
		}

	}

	/**
	 * Print using parameters
	 */
	private static void reportParams() {
		// TODO Auto-generated method stub
		System.out.println("Used data source: " + "CSVFile");
		System.out.println("Filename: " + file_name);
		System.out.println("Number bits in RAPPOR report: " + String.valueOf(numBits));
		System.out.println("Probability F: " + String.valueOf(probabilityF));
		System.out.println("Probability P: " + String.valueOf(probabilityP));
		System.out.println("Probability Q: " + String.valueOf(probabilityQ));
		System.out.println("Number cohorts: " + String.valueOf(numCohorts));
		System.out.println("Number Bloom Hashes: " + String.valueOf(numBloomHashes));
		System.out.println("Number Device: " + String.valueOf(numDevices));
		System.out.println("Number report by every device: " + String.valueOf(numReportPerDevice));
	}

	/**
	 * Parsing the command line arguments map
	 * @param argMap
	 */
	static void analiseCommandLine(HashMap<String, String> argMap) {
		if (argMap.containsKey(CL_ARG_MODE)) {
			mode = Integer.parseInt(argMap.get(CL_ARG_MODE));
		}
		if (argMap.containsKey(CL_ARG_FILE)) {
			file_name = argMap.get(CL_ARG_FILE);
		}
		if (argMap.containsKey(CL_ARG_NUM_BITS)) {
			numBits = Integer.parseInt(argMap.get(CL_ARG_NUM_BITS));
		}
		if (argMap.containsKey(CL_ARG_PROBABILITY_F)) {
			probabilityF = Double.parseDouble(argMap.get(CL_ARG_PROBABILITY_F));
		}
		if (argMap.containsKey(CL_ARG_PROBABILITY_P)) {
			probabilityP = Double.parseDouble(argMap.get(CL_ARG_PROBABILITY_P));
		}
		if (argMap.containsKey(CL_ARG_PROBABILITY_Q)) {
			probabilityQ = Double.parseDouble(argMap.get(CL_ARG_PROBABILITY_Q));
		}
		if (argMap.containsKey(CL_ARG_NUM_COHORT)) {
			numCohorts = Integer.parseInt(argMap.get(CL_ARG_NUM_COHORT));
		}
		if (argMap.containsKey(CL_ARG_NUM_BLOOM_HASHES)) {
			numBloomHashes = Integer.parseInt(argMap.get(CL_ARG_NUM_BLOOM_HASHES));
		}

		if (argMap.containsKey(CL_ARG_NUM_DEVICES)) {
			numDevices = Integer.parseInt(argMap.get(CL_ARG_NUM_DEVICES));
		}
		if (argMap.containsKey(CL_ARG_NUM_REPORT_PER_DEVICE)) {
			numReportPerDevice = Integer.parseInt(argMap.get(CL_ARG_NUM_REPORT_PER_DEVICE));
		}

	}

}
