/**
 * 
 */
package ru.digiratory.rappor;

import java.util.BitSet;
import java.util.HashMap;
import java.util.TreeSet;

/**
 * 
 * @author amsinitca@etu.ru Sinitca Aleksandr
 */
public class Dictionary {

	int numBit;

	/**
	 * 
	 */
	public Dictionary() {
		super();
		this.numBit = 8;
	}

	/**
	 * @param numBit
	 */
	public Dictionary(int numBit) {
		super();
		this.numBit = numBit;
	}

	HashMap<Integer, BloomTable> dictionary = new HashMap<>();
	TreeSet<String> values = new TreeSet<>();

	public synchronized void addValue(int cohort, String value, BitSet bloom) {
		if (!values.contains(value)) {
			values.add(value);
		}
		if (!dictionary.containsKey(new Integer(cohort))) {
			BloomTable tbl = new BloomTable(cohort);
			dictionary.put(new Integer(cohort), tbl);
		}
		dictionary.get(new Integer(cohort)).addValue(value, bloom);
	}

	public String toString() {
		// TODO ��������������� � ������
		StringBuilder str = new StringBuilder(5000);
		String[] vals = values.toArray(new String[values.size()]);
		for (int i = 0; i < values.size(); i++) {
			String tableHead = vals[i]; //"v" +  String.valueOf(i);
			str.append("\"" + tableHead + "\" ");
		}
		str.append("\n");		
		for (int coh = 0; coh < 2; coh++) { //TODO MAGIC NUMBER
			for (int i = 0; i < numBit; i++) {
				str.append("\"" + String.valueOf(i + numBit * coh + 1) + "\" ");
				for (int v = 0; v < values.size(); v++) {
					BloomTable tbl = dictionary.get(Integer.valueOf(coh));
					HashMap<String, BitSet> table = tbl.getTable();
					BitSet val = table.get(vals[v]);
					if (val != null) {
						boolean bit = val.get(i);
						str.append(String.valueOf(bit ? 1 : 0) + " ");
					} else {
						str.append(String.valueOf(0) + " ");
					}
				}
				str.append("\n");
			}
		}
		return str.toString();
	}

	class BloomTable {
		int cohort;
		HashMap<String, BitSet> table = new HashMap<>();

		public HashMap<String, BitSet> getTable() {
			return table;
		}

		/**
		 * @param cohort
		 */
		public BloomTable(int cohort) {
			super();
			this.cohort = cohort;
		}

		public void addValue(String str, BitSet value) {
			if (table.containsKey(str)) {
				if (!table.get(str).equals(value)) {
					// Warning
				}
			} else {
				table.put(str, value);
			}
		}

		public String toString() {
			// TODO ��������������� � ������
			return null;
		}

	}
}
