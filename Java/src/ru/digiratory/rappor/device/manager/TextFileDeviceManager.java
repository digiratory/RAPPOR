package ru.digiratory.rappor.device.manager;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
/**
 * 
 * @author Sinitca Aleksandr
 */
public class TextFileDeviceManager extends DeviceManager {
	FileWriter fos;

	/**
	 * 
	 */
	public TextFileDeviceManager(int numBit, String fileName) {
		super(numBit);
		File inputFile = new File(fileName);
		try {
			fos = new FileWriter(inputFile, false);
			fos.append("Cohort");
			fos.append(";");
			fos.append("Report");
			fos.append("\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**
	 * 
	 */
	public TextFileDeviceManager() {
		super();
		File inputFile = new File("data.csv");
		try {
			fos = new FileWriter(inputFile, false);
			fos.append("Cohort");
			fos.append(";");
			fos.append("Report");
			fos.append("\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void closeFile() {
		try {
			fos.flush();
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void sendBytes(int cohort, byte[] myByteArray, int start, int len) throws IOException {
		if (len < 0)
			throw new IllegalArgumentException("Negative length not allowed");
		if (start < 0 || start >= myByteArray.length)
			throw new IndexOutOfBoundsException("Out of bounds: " + start);
		if (len > 0) {
			StringBuilder reportStrBuilder = new StringBuilder(2 + 1 + len * 8 + 1);
			reportStrBuilder.append(Integer.toString(cohort));
			reportStrBuilder.append(";");
			byte[] output = new byte[len];
			if (myByteArray.length != len) {
				System.arraycopy(myByteArray, start, output, 0, len);
			} else {
				output = myByteArray;
			}

			for (int i = 0; i < output.length; i++) {
				for (int j = 7; j >= 0; j--) {
					if ((myByteArray[i] & (1 << j)) == (1 << j)) {
						reportStrBuilder.append("1");
					} else {
						reportStrBuilder.append("0");
					}
				}
			}
			reportStrBuilder.append("\n");
			fos.append(reportStrBuilder.toString());
		}

	}

}
