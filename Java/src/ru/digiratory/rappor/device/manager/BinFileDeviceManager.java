/**
 * 
 */
package ru.digiratory.rappor.device.manager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 
 * @author Sinitca Aleksandr
 */
public class BinFileDeviceManager extends DeviceManager {
	FileOutputStream fos;

	/**
	 * 
	 */
	public BinFileDeviceManager() {
		super();
		File inputFile = new File("data.bin");
		try {
			fos = new FileOutputStream(inputFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void sendBytes(int cohort, byte[] myByteArray, int start, int len) throws IOException {
		if (len < 0)
			throw new IllegalArgumentException("Negative length not allowed");
		if (start < 0 || start >= myByteArray.length)
			throw new IndexOutOfBoundsException("Out of bounds: " + start);
		if (len > 0) {
			fos.write(myByteArray, start, len);
		}

	}

}
