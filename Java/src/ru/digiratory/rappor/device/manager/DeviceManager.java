package ru.digiratory.rappor.device.manager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;

import ru.digiratory.rappor.Dictionary;
import ru.digiratory.rappor.device.Device;
import ru.digiratory.rappor.device.IDeviceListener;
/**
 * 
 * @author Sinitca Aleksandr
 */
public abstract class DeviceManager implements IDeviceListener {
	protected ArrayList<Device> devices = new ArrayList<Device>();
	protected Dictionary dictionary = null;
	protected int countRunningDevices = 0;
	
	
	
	/**
	 * 
	 */
	public DeviceManager() {
		super();
		dictionary = new Dictionary();
	}
	/**
	 * 
	 */
	public DeviceManager(int numBit) {
		super();
		dictionary = new Dictionary(numBit);
		
	}
	public int getCountRunningDevices() {
		return countRunningDevices;
	}

	private void sendBytes(int cohort, byte[] myByteArray) throws IOException {
		sendBytes(cohort, myByteArray, 0, myByteArray.length);
	}

	@Override
	public void onNewData(int cohort, String value, byte[] data, BitSet bloom) {
		dictionary.addValue(cohort, value, bloom);
		try {
			sendBytes(cohort, data);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStop() {
		countRunningDevices--;
		System.out.println("Running devices: " + String.valueOf(countRunningDevices));
	}
	
	public void addDevice(Device[] dev) {
		for (Device device : dev) {
			devices.add(device);
		}
	}

	public void startDevices() {
		for (Device device : devices) {
			device.start();
			countRunningDevices++;
		}
	}
	
	public String getDictionaryTable() {
		
		return dictionary.toString();
	}

	protected abstract void sendBytes(int cohort, byte[] myByteArray, int start, int len) throws IOException;
}
