/**
 * 
 */
package ru.digiratory.rappor.device.manager;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author siniz_000
 *
 */
public class SpeedTestDeviceManager extends DeviceManager {
	FileWriter fos;
	int counter = 0;
	long startTime = -1;
	/* (non-Javadoc)
	 * @see ru.digiratory.rappor.device.manager.DeviceManager#sendBytes(int, byte[], int, int)
	 */
	@Override
	protected synchronized void sendBytes(int cohort, byte[] myByteArray, int start, int len) throws IOException {
		// TODO Auto-generated method stub
		counter++;
		
		if (len < 0)
			throw new IllegalArgumentException("Negative length not allowed");
		if (start < 0 || start >= myByteArray.length)
			throw new IndexOutOfBoundsException("Out of bounds: " + start);
		if (len > 0) {
			if (startTime < 0) {
				startTime = System.currentTimeMillis();
			}
			if(System.currentTimeMillis() - startTime > 1000) {
				StringBuilder reportStrBuilder = new StringBuilder(2 + 1 + len * 8 + 1);
				reportStrBuilder.append(Integer.toString(counter));
				System.out.println(counter);
				reportStrBuilder.append(";");
				reportStrBuilder.append(countRunningDevices);
				reportStrBuilder.append("\n");
				fos.append(reportStrBuilder.toString());
				counter = 0;
				startTime = System.currentTimeMillis();
			}
		}
	}
	
	/**
	 * 
	 */
	public SpeedTestDeviceManager(int numBit, String fileName) {
		super(numBit);
		File inputFile = new File(fileName);
		try {
			fos = new FileWriter(inputFile, false);
			fos.append("N");
			fos.append(";");
			fos.append("Devices");
			fos.append("\n");	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void closeFile() {
		try {
			fos.flush();
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
