package ru.digiratory.rappor.device;

import java.util.BitSet;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import com.google.rappor.Encoder;

import ru.digiratory.datasource.DataSource;
/**
 * Virtual device 
 * @author amsinitca@etu.ru Sinitca Aleksandr
 *
 */
public class Device {
	int id;
	Encoder encoder;
	DataSource ds;
	int period_ms;
	int max_count;
	int counter = 0;
	int max_value = 8;
	boolean timerEnabled = false;
	Timer timer;

	IDeviceListener listener;

	/**
	 * ��������� ��������� ��������� ������ �� ���������: ����� � ����������� �����
	 * 
	 * @param id
	 *            -- id ����������
	 * @param encoder
	 *            -- ������� �������� ������
	 * @param ds
	 *            -- �������� ������
	 * @param period_ms
	 *            -- ������ ��������� ������
	 * @param max_count
	 *            -- ������������ ���������� ������
	 */
	public Device(int id, Encoder encoder, DataSource ds, int period_ms, int max_count) {
		super();
		this.id = id;
		this.encoder = encoder;
		this.ds = ds;
		this.period_ms = period_ms;
		this.max_count = max_count;
		this.listener = new IDeviceListener() {
			@Override
			public void onNewData(int cohort, String value, byte[] data, BitSet bloom) {
				System.out.print(id);
				System.out.print("--");
				System.out.println(data);
			}

			@Override
			public void onStop() {
				
				// TODO Auto-generated method stub
				
			}
		};
		timer = new Timer();
	}

	/**
	 * 
	 * @param id
	 *            -- id ����������
	 * @param encoder
	 *            -- ������� �������� ������
	 * @param ds
	 *            -- �������� ������
	 * @param period_ms
	 *            -- ������ ��������� ������
	 * @param max_count
	 *            -- ������������ ���������� ������
	 * @param listener
	 *            -- ��������� ��������� ��������� ������
	 */
	public Device(int id, Encoder encoder, DataSource ds, int period_ms, int max_count, IDeviceListener listener) {
		super();
		this.id = id;
		this.encoder = encoder;
		this.ds = ds;
		this.period_ms = period_ms;
		this.max_count = max_count;
		this.listener = listener;
		timer = new Timer();
	}

	void onNewData() {
		this.counter++;
		if (this.counter >= this.max_count) {
			timerEnabled = false;
			timer.cancel();
			listener.onStop();
		}
	}

	public void start() {
		timerEnabled = true;
		Random rnd = new Random();
		if (this.period_ms == 0) {
			timer.schedule(new TimerTask() {

				@Override
				public void run() {
					while (timerEnabled) {
						String str = ds.getNextString();
						byte[] data = encoder.encodeString(str);
						listener.onNewData(encoder.getCohort(), str, data, encoder.bloomFilterString(str));
						onNewData();
					}
				}
			}, id*100, 1);
		} else {
			timer.schedule(new TimerTask() {

				@Override
				public void run() {
					String str = ds.getNextString();
					byte[] data = encoder.encodeString(str);
					listener.onNewData(encoder.getCohort(), str, data, encoder.bloomFilterString(str));
					onNewData();
				}
			}, rnd.nextInt(20), this.period_ms);
		}

	}
}
