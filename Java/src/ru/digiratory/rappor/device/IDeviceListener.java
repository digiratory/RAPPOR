/**
 * 
 */
package ru.digiratory.rappor.device;

import java.util.BitSet;

/**
 * Device interface
 * 
 * @author Sinitca Aleksandr
 */
public interface IDeviceListener {
	/**
	 * New report sending by virtual device
	 * @param cohort
	 * @param value
	 * @param data
	 * @param bloom
	 */
	void onNewData(int cohort, String value, byte[] data, BitSet bloom);
	/**
	 * Virtual device stopped its work
	 */
	void onStop();
}
