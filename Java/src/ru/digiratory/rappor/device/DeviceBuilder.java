package ru.digiratory.rappor.device;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.google.rappor.Encoder;

import ru.digiratory.datasource.DataSource;
import ru.digiratory.rappor.EncoderConfig;

/**
 * 
 * @author Sinitca Aleksandr
 */
public class DeviceBuilder {

	public static Device[] getDevices(EncoderConfig encConfig, int count, String name, DataSource ds, int report_period,
			int report_num, IDeviceListener listener) {
		Device[] devices = new Device[count];
		for (int i = 0; i < count; i++) {
			Encoder encoder = new Encoder(makeTestingUserSecret(encConfig.getUserSecret() + "_" + String.valueOf(i)), // userSecret
					encConfig.getEncoderId()+ "_" + String.valueOf(i), // encoderId
					encConfig.getNumBits(), // numBits,
					encConfig.getProbabilityF(), // probabilityF
					encConfig.getProbabilityP(), // probabilityP
					encConfig.getProbabilityQ(), // probabilityQ
					encConfig.getNumCohorts(), // numCohorts
					encConfig.getNumBloomHashes()); // numBloomHashes;
			devices[i] = new Device(i, encoder, ds, report_period, report_num, listener);
		}
		return devices;
	}

	/**
	 * Convert a human readable string to a 32 byte userSecret for testing.
	 *
	 * <p>
	 * Do not use this in a production environment! For security, userSecret must be
	 * at least 32 bytes of high-quality entropy.
	 */
	private static byte[] makeTestingUserSecret(String testingSecret) {
		// We generate the fake user secret by concatenating three copies of the
		// 16 byte MD5 hash of the testingSecret string encoded in UTF 8.
		MessageDigest md5;
		try {
			md5 = MessageDigest.getInstance("MD5");
			byte[] digest = md5.digest(testingSecret.getBytes(StandardCharsets.UTF_8));
			// assertEquals(16, digest.length);
			return ByteBuffer.allocate(48).put(digest).put(digest).put(digest).array();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
