package ru.digiratory.datasource;

import java.security.SecureRandom;
/**
 * 
 * @author amsinitca@etu.ru Sinitca Aleksandr
 *
 */
public class DataSource {
	SecureRandom rnd;

	public DataSource() {
		rnd = new SecureRandom();
	}

	/**
	 * 
	 * @param max
	 *            -- ������������ �������� ��������� �����
	 * @return -- ��������� ����� �������� � ���������� ��������������
	 */
	@Deprecated
	public int getNextInt(int max) {
		int v = 0;
		for (int i = 0; i < 6; i++) {
			v += rnd.nextInt(max);
		}
		v /= 6;
		return v;
	}

	/**
	 * 
	 * @return -- ��������� ����� �������� � ���������� ��������������
	 */
	public synchronized String getNextString() {
		int v = 0;
		for (int i = 0; i < 6; i++) {
			v += rnd.nextInt(8);
		}
		v /= 6;
		switch (v) {
		case 0:
			return "String_0";
		case 1:
			return "String_1";
		case 2:
			return "String_2";
		case 3:
			return "String_3";
		case 4:
			return "String_4";
		case 5:
			return "String_5";
		case 6:
			return "String_6";
		case 7:
			return "String_7";

		default:
			return "String_3";
		}
	}

}
