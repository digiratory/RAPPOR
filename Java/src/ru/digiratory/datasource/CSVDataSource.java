package ru.digiratory.datasource;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * 
 * @author amsinitca@etu.ru Sinitca Aleksandr
 *
 */
public class CSVDataSource extends DataSource {
	String fileName;
	int dataColNum;
	BufferedReader fileReader;
	
	/**
	 * @param fileName
	 */
	public CSVDataSource(String fileName, int dataColNum) {
		super();
		this.fileName = fileName;
		this.dataColNum = dataColNum;
		createFileReader();
	}
	
	protected void createFileReader() {
		try {
			fileReader = new BufferedReader(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getNextString() {
		try {
			if(fileReader.ready()) {
				String line = fileReader.readLine();				
				if (line != null) {
					String[] cols = line.split(",");
					return cols[dataColNum];
				}else {
					createFileReader();
					String[] cols = fileReader.readLine().split(",");
					return cols[dataColNum];
				}
				
			}else {
				createFileReader();
				String[] cols = fileReader.readLine().split(",");
				return cols[dataColNum];
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void closeDataSource() {
		try {
			fileReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
